<?php

function checkLogin($email, $password, $conn) {

    if ($email) {
        $email = trim($email);
        $email = stripslashes($email);
        $email = htmlspecialchars($email);
        $email = strtolower($email);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
           return $err['error'] = "Format d'email invalide";
        }

    } else {
        return $err['error'] = "Format d'email invalide";
    }

    if (!$password || empty($password)) {
        return $err['error'] = "Mot de passe invalide";
    } 


    $stmt = $conn->prepare("SELECT * FROM clients where email= '$email' and password='".hash('sha256', $password)."'");
    $stmt->execute();

    if ($stmt->rowCount() > 0) {
        // create session

       $infos= $stmt->fetch(PDO::FETCH_OBJ);

       $_SESSION["user"] = $infos->firstName.' '.$infos->lastName;
       $_SESSION["id"] = $infos->id;

       header("location: index.php");
       exit();
        
    } else {
        return $err['error'] = "Les informations sont incorrectes";
    }
}





function getProducts($conn, $category = null) {

  $stmt = "";

  if ($category !== null) {
    $stmt = $conn->prepare("SELECT ref, name, type, price, shipping, description, manufacturer, image  FROM products JOIN category_produit ON products.ref = category_produit.product_id WHERE category_produit.category_id = '$category'");
  } else {
    $stmt = $conn->prepare("SELECT ref, name, type, price, shipping, description, manufacturer, image  FROM products");
  }

  $stmt->execute();
  $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
  echo json_encode($stmt->fetchAll());
    

}


function getCategories($conn) {

  $stmt = "";
  $stmt = $conn->prepare("SELECT categories.name as name, categories.id as id, count(products.ref) as 'count' FROM categories INNER JOIN category_produit ON categories.id = category_produit.category_id INNER JOIN products ON products.ref = category_produit.product_id GROUP BY (name)");


  $stmt->execute();
  $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
  echo json_encode($stmt->fetchAll());
    

}


function createOrder($conn) {

  $sql = "INSERT INTO orders (client_id, date) VALUES (".$_SESSION['id'].", NOW())";
  $conn->exec($sql);

  $order_id = $conn->lastInsertId();


  if (isset($_SESSION['cartItems'])) {
      foreach ($_SESSION['cartItems'] as $item) {
        $sql = "INSERT INTO orders_products (order_id, product_id, qte) VALUES (".$order_id.",".$item->ref.",".$item->qte.")";
        $conn->exec($sql);
      }
  }

  $_SESSION['cartItems'] = array();

  $data['items'] = $_SESSION['cartItems'];
  $data['itemsCount'] = count($_SESSION['cartItems']);
  
  echo json_encode($data);

}


function checkEmailUnique($email, $id, $conn) {

  if(!is_null($id)) $stmt = $conn->prepare("SELECT count(email) as count FROM clients where email='".$email."' and id <> ".$id);
  else $stmt = $conn->prepare("SELECT count(email) as count FROM clients where email='".$email."'");
  $stmt->execute();

  //error_log(json_encode($stmt->fetch(PDO::FETCH_OBJ)));

  if ($stmt->rowCount() > 0) {

    $infos= (object) $stmt->fetch(PDO::FETCH_OBJ);


    error_log(json_encode($infos));
    error_log(json_encode($infos-> count));

   if ($infos->count > 0) {
        return false;
   } else {
     return true;
   }
 }

}



function createClient($user, $conn) {


  if (!checkEmailUnique($user['email'],null, $conn)) {
    $err  = $user;
    unset($err['password']);
    if (isset($err['newpassword'])) unset($err['newpassword']);
    $err['error'] = "Cet adresse mail existe déja! ";
    return $err;
  }


    try {

        $sql = "INSERT INTO clients (firstName, lastName, email, password)
        VALUES ('".$user['firstName']."', '".$user['lastName']."', '".$user['email']."','".hash('sha256', $user['password'])."')";
        error_log(json_encode($sql));
        $conn->exec($sql);


        $_SESSION["user"] = $user['firstName'].' '.$user['lastName'];
        $_SESSION["id"] = $conn->lastInsertId();
        header("location: index.php");
        exit();

    } catch(PDOException $e) {

       echo $sql . "<br>" . $e->getMessage();
    }

}


  function checkSessionLogin() {

    session_start();

    if (isset($_SESSION['user']) && !empty($_SESSION['user'])) {
       header("location: index.php");
       exit();
     } 
  }

  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "boutique";


  try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname;charset=utf8", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  } catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
  }



?>
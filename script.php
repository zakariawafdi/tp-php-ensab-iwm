<?php


include 'db/db.php';

function fillCategories($conn) {
    $strJsonFileContents = file_get_contents("./data/categories.json");
    // Convert to array 
    $categories = json_decode($strJsonFileContents, true);
   // var_dump($array); // print array

    if ($categories && sizeof($categories) > 0) {
      foreach ($categories as $category) {
         $sql = "INSERT INTO `categories` (`id`, `name`) values (:id, :name)";
         $sql = $conn->prepare($sql);
         $sql->bindValue(':id', $category['id'], PDO::PARAM_STR );
         $sql->bindValue(':name', $category['name'], PDO::PARAM_STR );
         $sql->execute();
        
      }
    }
    echo "done fill";
  }

  function fillProducts($conn) {
    $strJsonFileContents = file_get_contents("./data/products.json");
    // Convert to array 
    $products = json_decode($strJsonFileContents, true);
   // var_dump($array); // print array

    if ($products && sizeof($products) > 0) {
      foreach ($products as $product) {
         $sql = "INSERT INTO `products` (`ref`, `name`, `type`, `price`, `shipping`, `description`, `manufacturer`,
         `image`) values (:ref, :name, :type, :price, :shipping, :description, :manufacturer, :image)";
         $sql = $conn->prepare($sql);
         $sql->bindValue(':ref', $product['ref'], PDO::PARAM_INT );
         $sql->bindValue(':name', $product['name'], PDO::PARAM_STR );
         $sql->bindValue(':type', $product['type'], PDO::PARAM_STR );
         $sql->bindValue(':price', (string)$product['price'], PDO::PARAM_STR);
         $sql->bindValue(':shipping', (string)$product['shipping'], PDO::PARAM_STR);
         $sql->bindValue(':description', $product['description'], PDO::PARAM_STR );
         $sql->bindValue(':manufacturer', $product['manufacturer'], PDO::PARAM_STR );
         $sql->bindValue(':image', $product['image'], PDO::PARAM_STR );
         $stmt =  $sql->execute();
        if ($stmt) {
          foreach($product['category'] as $key=>$value) {

            $sql = "INSERT INTO `category_produit` ( `product_id`, `category_id`) values (:product_id, :category_id)";
            $sql = $conn->prepare($sql);
            $sql->bindValue(':product_id', $product['ref'], PDO::PARAM_INT );
            $sql->bindValue(':category_id', $product['category'][$key]['id'], PDO::PARAM_STR );
            $sql->execute();
          }
          
 
        }

        
      }
    }
    echo "done fill";
  }


  fillCategories($conn);
  fillProducts($conn);


?>
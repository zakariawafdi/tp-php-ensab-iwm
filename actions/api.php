<?php 

include '../db/db.php';

session_start();


// service pour ajout d'un article au panier

if ($_GET['service'] == "addTocart") {

    if (!isset($_SESSION['cartItems']) || empty($_SESSION['cartItems'])) {
        $_SESSION['cartItems'] = array();
        array_push($_SESSION['cartItems'], json_decode(file_get_contents('php://input')));
    } else {
        array_push($_SESSION['cartItems'], json_decode(file_get_contents('php://input')));
    }
    
    $data['items'] = $_SESSION['cartItems'];
    $data['itemsCount'] = count($_SESSION['cartItems']);
    
    echo json_encode($data);
}

// service pour recupérer les produits
// tout les produits si le un parametre n'est pas envoyé
// les produits par une catégrie si le un parametre envoyé

if ($_GET['service'] == "fetchProducts") {
    
    if (isset($_GET['category'])) {
        getProducts($conn, $_GET['category']);
    } else {
        getProducts($conn);
    }
}


// service pour recupérer toutes les categories (id, nom et nombre de produits)

if ($_GET['service'] == "fetchCategories") { 
    getCategories($conn);
}


// service pour modifier le panier 

if ($_GET['service'] == "updateCart") { 
    $_SESSION['cartItems'] = array();
    $_SESSION['cartItems'] = json_decode(file_get_contents('php://input'));

    $data['items'] = $_SESSION['cartItems'];
    $data['itemsCount'] = count($_SESSION['cartItems']);

    echo json_encode($data);
}


// récupérer les elements du panier depuis la variable superglobale 'session'

if ($_GET['service'] == "fetchCart") { 
    

    if (!isset($_SESSION['cartItems'])) {
        $_SESSION['cartItems'] = array();
    }

    $data['items'] = $_SESSION['cartItems'];
    $data['itemsCount'] = count($_SESSION['cartItems']);

    echo json_encode($data);
}

// Vérifier si l'utilisateur est connecté

if ($_GET['service'] == "checkUserConnected") { 
    
    $data = array();

    if (!isset($_SESSION['user'])) {
        $data['connected'] = false;
    } else {
        $data['connected'] = true;
    }

    echo json_encode($data);
}


if ($_GET['service'] == "createOrder") { 
    createOrder($conn);
}




?>
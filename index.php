<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="lib/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
    <script src="lib/angular-datatables.min.js"></script>
    <script src="lib/jquery.dataTables.min.js"></script>
    <script src="lib/ui-bootstrap-tpls-0.14.3.min.js"></script>

    <link rel="stylesheet" href="lib/bootstrap.min.css">
    <link rel="stylesheet" href="lib/datatables.bootstrap.css">
    <link href="lib/style/dashboard.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css" rel="stylesheet">

    <title>Hello, world!</title>
  </head>


  <body ng-app="productsApp" ng-controller="productController">
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Tp Ensab shop</a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" ><span data-feather="shopping-cart" ng-click="goToCart()"></span><span class="badge badge-secondary">{{ itemsCount }}</span></a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link active" href="index.php">
                  toutes les produits</span>
                </a>
              </li>
              <li class="nav-item" ng-repeat = "category in categories track by $index">
                <a class="nav-link" href="index.php?category={{category.id}}">
                  {{category.name}} <span class="badge badge-secondary">{{ category.count }}</span>
                </a>
              </li>
            </ul>
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            <h1 class="h2">Produits</h1>
          </div>

          <div class="table-responsive">
            <table datatable="ng" dt-option="vm.dtOptions" class="table table-striped table-bordered table-sm">

            <thead>
                  <tr>
                      <th class="font-weight-semi-bold border-top-0 py-2">#</th>
                      <th class="font-weight-semi-bold border-top-0 py-1">Nom</th>
                      <th class="font-weight-semi-bold border-top-0 py-2">Type</th>
                      <th class="font-weight-semi-bold border-top-0 py-2">Prix</th>
                      <th class="font-weight-semi-bold border-top-0 py-2">Livraison</th>
                      <th class="font-weight-semi-bold border-top-0 py-2">Description</th>
                      <th class="font-weight-semi-bold border-top-0 py-2">Manufacturer</th>
                      <th class="font-weight-semi-bold border-top-0 py-2">Image</th>
                      <th class="font-weight-semi-bold border-top-0 py-2">Actions</th> 
                  </tr>
              </thead>
              <tbody>
                  <tr ng-repeat = "product in products track by $index">
                      <td>{{product.ref}}</td>
                      <td>{{product.name}}</td>
                      <td>{{product.type}}</td>
                      <td>{{product.price}}</td>
                      <td>{{product.shipping}}</td>
                      <td>{{product.description}}</td>
                      <td>{{product.manufacturer}}</td>
                      <td><img width="30" src="{{product.image}}" alt="{{product.image}}"></td>
                      <td><span data-feather="plus-square" ng-click="addToCart(product)"></span></td>


                  </tr>

              </tbody>

            </table>
        </div>

        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->

    <script src="lib/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>

    
  </body>

    <script>
        var app = angular.module('productsApp', ['datatables', 'ui.bootstrap', 'ui.bootstrap.modal']);

        app.controller('productController', function ($scope, $http, $uibModal) {
            $scope.itemsCount = 0;

            var qte = 1;

            var items = [];

            $http.get("actions/api.php?service=fetchProducts<?php echo isset($_GET['category'])? '&category='.$_GET['category']: '' ?>").success((data, status, headers, config) => {
                // console.log(data);
              $scope.products = data;
            });

            $http.get("actions/api.php?service=fetchCart").success((data, status, headers, config) => {
                $scope.items = data.items;
                items = data.items;
                $scope.itemsCount = data.itemsCount;
            });

            $http.get("actions/api.php?service=fetchCategories").success((data, status, headers, config) => {
              $scope.categories = data;
            });


            $scope.addToCart = async (item)  => {
              
                let modalPromise = new Promise(function(myResolve, myReject) {
                    $uibModal.open({
                        templateUrl: 'lib/modals/modalqte.html',
                        backdrop: true,
                        windowClass: 'modal',
                        controller: function ($scope, $uibModalInstance, $log)  {
                            $scope.qte = qte;
                            $scope.product = item.name;
                            $scope.submit = () => {
                                $log.log('Quantity');
                                $log.log($scope.qte);
                                item.qte = $scope.qte;
                                $uibModalInstance.dismiss('cancel');
                                myResolve(item);
                            }
                            $scope.cancel = () => {
                              myReject(true);
                              $uibModalInstance.dismiss('cancel');
                            };
                        },
                        resolve: {
                            item: () => {
                            }
                        }
                    });
    
                });


                modalPromise.then(function(data) {
                    $http.post('actions/api.php?service=addTocart', data).then((response) => {
                        if (response.data) {
                            $scope.items = response.data.items;
                            items = response.data.items;
                            $scope.itemsCount = response.data.itemsCount;
                        } 
                    });    

                }, function(error) {
                   return false;
                })
            

            };
            
            
            $scope.goToCart = async () => {

                let modalPromise2 = new Promise(function(myResolve, myReject) {
                        $uibModal.open({
                            templateUrl: 'lib/modals/cart.html',
                            backdrop: true,
                            windowClass: 'modal-cart',
                            controller: function($scope, $uibModalInstance, $log) {
                                console.log('items go to', items);
                                $scope.products = items;
                                $scope.removeItem = function (scopeItem) {
                                    var index = $scope.products.indexOf(scopeItem);
                                    $scope.products.splice(index, 1); 
                                };
                                $scope.submit = function() {
                                    $log.log($scope); 
                    
                                    $uibModalInstance.dismiss('cancel');
                                    myResolve($scope.products);
                                }
                                $scope.cancel = function() {
                                    $uibModalInstance.dismiss('cancel');
                                    myReject(true);
                                };
                            },
                            resolve: {
                                items: function() {
                            
                                }
                            }
                        });

                });

                modalPromise2.then(function(data) {
                    $http.post('actions/api.php?service=updateCart', data).then(function(response) {
                    if (response.data) {
                        $scope.items = response.data.items;
                        $scope.itemsCount = response.data.itemsCount;
                    }

                    $http.get('actions/api.php?service=checkUserConnected').success((data, status, headers, config) => {
                        if (data.connected !== true) {
                            $uibModal.open({
                                templateUrl: 'lib/modals/login.html',
                                backdrop: true,
                                windowClass: 'modal3',
                                controller: function ($scope, $uibModalInstance, $log)  {
                                    $scope.goLogin = () => {
                                        $uibModalInstance.dismiss('cancel');
                                        window.location.href = '/login.php'
                                    };
                                    $scope.goSignup = () => {
                                        $uibModalInstance.dismiss('cancel');
                                        window.location.href = '/signup.php'
                                    };
                                },
                                resolve: {
                                    item: () => {}
                                }
                            });
                        } else {

                            $http.get('actions/api.php?service=createOrder').success((data, status, headers, config) => {
                                 location.reload()
                            });
                                
                        }
                    });
                });
                });

            };
        });
    </script>
  
</html>
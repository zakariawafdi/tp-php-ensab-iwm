<?php

include 'db/db.php';



checkSessionLogin();

if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] === "POST") {
	$err = checkLogin($_POST['email'],$_POST['password'], $conn);
}
?>


<!DOCTYPE html>
<html lang="fr">
  <head>
    <!-- Title -->
    <title>Login | TP ENSAB SHOP</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="public/img/favicon.ico">

    <!-- Template -->
    <link rel="stylesheet" href="lib/style/login-signup.css">
  </head>

  <body class="">

    <main class="main">

      <div class="content">

			<div class="container-fluid pb-5">

				<div class="row justify-content-md-center">
					<div class="card-wrapper col-12 col-md-4 mt-5">
						<div class="brand text-center mb-3">
							<a href="/">TP PHP ENSAB</a>
						</div>
						<div class="card">
							<div class="card-body">
								<h4 class="card-title">Login</h4>
								<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>">
									<div class="form-group">
										<label for="email">Adresse e-mail</label>
										<input id="email" type="email" class="form-control" name="email" value="<?php echo isset($_REQUEST['email'])? $_REQUEST['email']: ''?>" required="" autofocus="">
									</div>

									<div class="form-group">
										<label for="password">Mot de passe
										</label>
										<input id="password" type="password" class="form-control" name="password" required="">
									</div>

									<div class="form-group no-margin">
									    <?php echo isset($err)? "<b style='color: red'>".$err."</b><br>": "" ?>
										<input type="submit" value="Se connecter"  class="btn btn-primary btn-block">
									</div>
								</form>
							</div>
						</div>
						<footer class="footer mt-3">
							<div class="container-fluid">
								<div class="footer-content text-center small">
									<span class="text-muted">&copy; Tp Ensab. Développé par Zakaria WAFDI</a></span>
								</div>
							</div>
						</footer>
					</div>
				</div>



			</div>

      </div>
    </main>

  </body>
</html>